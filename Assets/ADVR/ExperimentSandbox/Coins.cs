﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour
{
    public int numCoins = 0;
    public TextMesh text;

    // Use this for initialization
    void Start()
    {
        RewardNotifications.OnAdReward += RewardNotifications_OnAdReward;
        SetText();
    }

    private void RewardNotifications_OnAdReward()
    {
        numCoins += 5;
        SetText();
    }

    // Update is called once per frame
    private void SetText()
    {
        text.text = "Coins: " + numCoins;
    }
}
