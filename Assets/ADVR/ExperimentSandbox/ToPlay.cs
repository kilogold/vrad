﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This is a little hack script to test and demonstrate how we can load assets from 
/// persistent datapath. Mostly due to quirks with EasyMovieTexture.
/// </summary>
public class ToPlay : MonoBehaviour 
{
    public MediaPlayerCtrl player;
	// Use this for initialization
	void Awake () 
	{
        player.m_strFileName = Application.persistentDataPath + "/GData/DROPBOX_1r3sqHt_oJAAAAAAAAAAAQ.mp4";
#if UNITY_EDITOR_WIN
        player.m_strFileName = player.m_strFileName.Replace(":/", "://");
#elif UNITY_ANDROID
        player.m_strFileName = player.m_strFileName.Insert(0, "file://");
#endif
    }
}
