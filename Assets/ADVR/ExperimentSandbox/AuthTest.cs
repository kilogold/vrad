using UnityEngine;
using System.Collections;

public class AuthTest : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(Token());
    }

    // Use this for initialization
    IEnumerator UserCode()
    {
        WWWForm form = new WWWForm();
        form.AddField("client_id","670746440507-b0083l4ojemm9etn396h923tvqupl9hp.apps.googleusercontent.com");
        form.AddField("scope", "email");
        WWW www = new WWW( "https://accounts.google.com/o/oauth2/device/code", form);

        yield return www;

        Debug.Log(www.text);
    }

    // Update is called once per frame
    IEnumerator Token()
    {
        while (true)
        {
            WWWForm form = new WWWForm();
            form.AddField("client_id", "670746440507-b0083l4ojemm9etn396h923tvqupl9hp.apps.googleusercontent.com");
            form.AddField("client_secret", "FYz30kOrDS8bCiuwo2V59LhZ");
            form.AddField("code", "SWET-WHSU4/vYn3sBxy5lO5ezn_rasuGpIPx2WgKzHkfx0fkEXFXfs");
            form.AddField("grant_type", "http://oauth.net/grant_type/device/1.0");
            WWW www = new WWW("https://www.googleapis.com/oauth2/v3/token", form);

            yield return www;

            Debug.Log(www.text);

            yield return new WaitForSeconds(5);
        }
    }
}

/*
{
  "device_code" : "SWET-WHSU4/vYn3sBxy5lO5ezn_rasuGpIPx2WgKzHkfx0fkEXFXfs",
  "user_code" : "SWET-WHSU",
  "verification_url" : "https://www.google.com/device",
  "expires_in" : 1800,
  "interval" : 5
}
*/
