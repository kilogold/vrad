﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using Boomlagoon.JSON;
using System;

public class DropboxLoader : ContentLoader 
{

    [SerializeField]
    private string accessToken = "ma1Biav3Y-YAAAAAAAADg2m7KuHeuHplIKolMqWnjvvWSIAMMQECnQ9y4JfHPfQX";

    private string urlDownload = "https://content.dropboxapi.com/2/files/download";
    private string urlListFolder = "https://api.dropboxapi.com/2/files/list_folder";
    private string urlDatabase = "http://54.201.100.73/queryDB.php";

    protected override void Awake()
    {
        loaderFileHashkey = "DROPBOX_";
        base.Awake();
    }

    /// <summary>
    /// Donwloads the file and saves it to disk for AdRepo to load.
    /// </summary>
    /// <param name="fileObj"></param>
    /// <param name="filename">The filename to be written out to disk.</param>
    /// <returns></returns>
    IEnumerator DownloadToDisk( JSONObject fileObj, string filename )
    {
        string fileID = fileObj.GetValue("id").Str;
        string apiArg = "{\"path\": \"" + fileID + "\" }";
        
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Authorization", "Bearer " + accessToken);
        headers.Add("Dropbox-API-Arg", apiArg);

        WWW wwwDropbox = new WWW(urlDownload,null, headers);

        Debug.Log("Downloading: " + fileObj.GetValue("name").Str);
        while(!wwwDropbox.isDone)
        {
            Debug.Log("Download " + wwwDropbox.progress + "%");
            yield return null;
        }
        Debug.Log("Finished Downloading: " + fileObj.GetValue("name").Str);

        if (!string.IsNullOrEmpty(wwwDropbox.error))
        {
            Debug.LogError(wwwDropbox.error);
        }

        string timestampLastUpdateString = fileObj.GetValue("client_modified").Str;

        DateTime timestampLastUpdate = DateTime.Parse(timestampLastUpdateString);

        fileIO.WriteToDisk(filename, wwwDropbox.bytes, timestampLastUpdate);

        wwwDropbox.Dispose();
    }

    public override IEnumerator LoadContent()
    {
        // Before we start all the storage/Dropbox downloading, let's obtain Gumdrop-specific metadata.
        // Here we can have access to the content's theme information.
        WWW wwwGumdropDB = new WWW(urlDatabase);
        yield return wwwGumdropDB;
        JSONArray gumdropDB = JSONArray.Parse(wwwGumdropDB.text);
        wwwGumdropDB.Dispose();

        // Now we can begin downloading everything from Dropbox!
        string dataString = @"{ 
                                ""path"": """",
                                ""recursive"": false,
                                ""include_media_info"": false,
                                ""include_deleted"": false,
                                ""include_has_explicit_shared_members"": false
                            }";
        Dictionary<string,string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        headers.Add("Authorization", "Bearer " + accessToken);

        byte[] body = Encoding.UTF8.GetBytes(dataString);

        WWW wwwListFolder = new WWW(urlListFolder, body,headers);

        yield return wwwListFolder;

        if(!string.IsNullOrEmpty(wwwListFolder.error))
        {
            Debug.LogError(wwwListFolder.error);
        }

        JSONObject json = JSONObject.Parse(wwwListFolder.text);

        wwwListFolder.Dispose();

        var entries = json.GetArray("entries");

        string[] contentFilenameList = new string[entries.Length];
        string[] contentUrlList = new string[entries.Length];
        string[] contentTitleList = new string[entries.Length];
        string[] contentUniqueIdList = new string[entries.Length];
        int[] contentThemeMaskList = new int[entries.Length];

        for (int curIndex = 0; curIndex < entries.Length; curIndex++)
        {          
            JSONObject entryObj = entries[curIndex].Obj;

            string fileID = entryObj.GetValue("id").Str;
            string contentUniqueID = fileID.Substring(3);// removes "id:" from string.
            string originalFilename = entryObj.GetValue("name").Str;
            string hashedFilename = GenerateFilenameHash(contentUniqueID, originalFilename);
            int themeMask = 0;

            if( false == fileIO.DoesFileExist(hashedFilename))
                yield return StartCoroutine(DownloadToDisk(entryObj, hashedFilename));

            foreach (var entry in gumdropDB)
            {
                if(entry.Obj.GetString("uid").Equals(contentUniqueID))
                {
                    themeMask = (int)entry.Obj.GetNumber("theme");
                    break;
                }
            }

            contentFilenameList[curIndex] = hashedFilename;
            contentUrlList[curIndex] = urlDownload + entryObj.GetValue("path_lower").Str;
            contentUniqueIdList[curIndex] = contentUniqueID;
            contentTitleList[curIndex] = originalFilename;
            contentThemeMaskList[curIndex] = themeMask;
        }

        RaiseContentLoadingCompleteEvent(
            new ContentLoadingCompleteEventArgs(
                    contentFilenameList,
                    contentUrlList, 
                    contentUniqueIdList, 
                    contentTitleList,
                    contentThemeMaskList ) );
    }

}
