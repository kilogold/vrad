using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System;

[System.Serializable]
public class PicasaLoader : ContentLoader
{
    [SerializeField]
    private string picasaUserAlbumId = "6155259190267990577";

    [SerializeField]
    private string picasaUserId = "101867488510872520535";

    private string picasaGetAlbumUrl = string.Empty;

    // Temp placeholder to store loaded data to return from LoadContent.

    protected override void Awake()
    {
        loaderFileHashkey = "PICASA_";
        picasaGetAlbumUrl = "https://picasaweb.google.com/data/feed/api/user/" + picasaUserId + "/albumid/" + picasaUserAlbumId;
        base.Awake();
    }

    public override IEnumerator LoadContent()
    {
        WWW request = new WWW(picasaGetAlbumUrl);

        yield return request;

        XmlDocument doc = new XmlDocument();

        doc.Load(new StringReader(request.text));
        request.Dispose();

        //Create an XmlNamespaceManager for resolving namespaces.
        XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
        nsmgr.AddNamespace("atom", "http://www.w3.org/2005/Atom");
        nsmgr.AddNamespace("openSearch", "http://a9.com/-/spec/opensearchrss/1.0/");
        nsmgr.AddNamespace("gphoto", "http://schemas.google.com/photos/2007");
        nsmgr.AddNamespace("exif", "http://schemas.google.com/photos/exif/2007");
        nsmgr.AddNamespace("media", "http://search.yahoo.com/mrss/");

        XmlNodeList nodeListEntries = doc.DocumentElement.SelectNodes("/atom:feed/atom:entry", nsmgr);

        string[] contentFilenameList = new string[nodeListEntries.Count];
        string[] contentUrlList = new string[nodeListEntries.Count];
        string[] contentTitleList = new string[nodeListEntries.Count];
        string[] contentUniqueIdList = new string[nodeListEntries.Count];

        for (int curNodeIndex = 0; curNodeIndex < nodeListEntries.Count; curNodeIndex++)
        {
            // Let's begin parsing the node...
            // First, we should get the unique id of the content we intend on downloading (found within node data).
            XmlNode entryImageIdNode = nodeListEntries[curNodeIndex].SelectSingleNode("gphoto:id", nsmgr);
            string contentUniqueID = entryImageIdNode.InnerText;

            XmlNode entryTitleNode = nodeListEntries[curNodeIndex].SelectSingleNode("atom:title", nsmgr);
            string entryTitle = entryTitleNode.InnerText;


            // We now have the unique id, so let's fire the WillLoadEvent to verify with all gate-keepers (possibly only AdRepo)
            // about whether we have a duplicate already loaded. If so, let's not bother loading this content.
            // TODO:
            // THIS CHECK IS OBSOLETE. THE NEW STRUCTURE ENFORCES LOADERS TO ALWAYS DOWNLOAD TO DISK.
            // THE ADREPO WILL THEN LOAD FROM DISK TO MEMORY AS-NEEDED. REMOVE SOON.
            bool shouldLoad = false;
            RaiseWillLoadEvent(ref shouldLoad, ref contentUniqueID);

            if (false == shouldLoad)
            {
                continue;
            }

            // Video entries have 2 media elements (thumbnail and video).
            // We always assume there is a video node. If there isn't, we know it's an image.
            // Source-- https://developers.google.com/picasa-web/docs/2.0/reference#media_reference
            XmlNode entryContentNode = nodeListEntries[curNodeIndex].SelectSingleNode("media:group/media:content[@medium=\"image\"]", nsmgr);
            if( entryContentNode == null )
            { 
                /*****************************************************************************************
                Turns out we CAN'T download videos from Picasa----
                    
                    Limitations

                    All uploaded videos are processed and converted into streaming formats that can 
                    be played back using the Flash video player integrated into the Picasa Web Albums one-up view.
                    Two streaming formats are currently created: the H.263-based FLV320 with a maximum 
                    resolution of 320 by 240 pixels, and an mp4 stream with a maximum resolution of 480 x 360 pixels.
                    In both cases, the maximum upload size is 100MB.
                    You can't currently download original video files. However, you can download the 
                    FLV video stream or the mp4 video stream by using the URLs from the <media:content> video elements 
                    provided in the photo entry or feed. There's one <media:content> element for each video stream.
                
                    https://developers.google.com/picasa-web/docs/2.0/developers_guide_protocol#PostVideo

                There is a mess between what is Picasa, Google+, Google Drive, and Google Photos.
                I can either deal with this madness or write another ContentLoader for a different hosting service.
                For now, let's not load videos.
                *************************************************************************************************/
                Debug.LogError("Invalid entry (possibly video?). Remove from Picasa.");
                continue;
            }

            string contentUrl = entryContentNode.Attributes["url"].InnerText;
            string uniqueFilename = GenerateFilenameHash(contentUniqueID, entryTitle);

            XmlNode entryDateNode = nodeListEntries[curNodeIndex].SelectSingleNode("atom:updated", nsmgr);
            string contentLastUpdated = entryDateNode.InnerText;

            DateTime contentLastUpdatedTime = DateTime.Parse(contentLastUpdated);
            bool shouldWriteNewContent = false;

            // If the file exists, and has not been updated server-side,
            // there's no point in downloading. We use what is already cached.
            if (fileIO.DoesFileExist(uniqueFilename))
            {
                DateTime contentLastUpdatedTimeFromFile = fileIO.GetServerTimestamp(uniqueFilename);
                int dateDifference = DateTime.Compare(contentLastUpdatedTime, contentLastUpdatedTimeFromFile);
                bool areDatesIdentical = (dateDifference == 0);

                if (false == areDatesIdentical)
                {
                    shouldWriteNewContent = true;
                }
            }
            else
            {
                shouldWriteNewContent = true;
            }

            if (shouldWriteNewContent)
            {
                WWW www = new WWW(contentUrl);

                yield return www;

                fileIO.WriteToDisk(uniqueFilename, www.bytes, contentLastUpdatedTime);

                www.Dispose();

                contentFilenameList[curNodeIndex] = uniqueFilename;
                contentUrlList[curNodeIndex] = contentUrl;
                contentUniqueIdList[curNodeIndex] = contentUniqueID;
                contentTitleList[curNodeIndex] = entryTitle;
            }
        }

        //HACK: TODO:
        // Make this a better structure by making a class to store all content & metadata.
        // We can then use List<> to increment the amount we are intending to load.
        bool didObtainNewContent = false;
        foreach (var item in contentFilenameList)
        {
            if (item != null)
            {
                didObtainNewContent = true;
                break;
            }
        }
        if (didObtainNewContent)
            RaiseContentLoadingCompleteEvent(new ContentLoadingCompleteEventArgs(contentFilenameList, contentUrlList, contentUniqueIdList, contentTitleList));
    }
}
