using UnityEngine;
using System.Collections;
using System;

public class TextureAd : Ad
{
    Texture adTexture;
    AdCore adCore;

    AdContentInfo Ad.Info
    {
        get { return adCore.AdContent.Info; }
    }

    event Action Ad.AdExpired
    {
        add { adCore.AdExpired += value; }
        remove { adCore.AdExpired -= value; }
    }

    public TextureAd(AdContentTexture adContentIn, Material adSurfaceMaterialIn, float lifetime)
    {
        adCore = new AdCore(adContentIn, adSurfaceMaterialIn, lifetime);
        adTexture = adContentIn.ContentTexture;
    }

    public void Play()
    {
        adCore.Play();
        adCore.AdSurfaceMaterial.mainTexture = adTexture;
    }

    public void Stop()
    {
        adCore.Stop();
        adCore.AdSurfaceMaterial.mainTexture = adCore.OriginalTexture;
    }

    public void Pause()
    {
        adCore.Pause();
    }

    public void Tick(float deltaTime)
    {
        adCore.Tick(deltaTime);
    }
}
