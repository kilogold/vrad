﻿using UnityEngine;

/// <summary>
/// Required for custom property drawer
/// </summary>
public class EnumFlagAttribute : PropertyAttribute { }