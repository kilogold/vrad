﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class ContentFileIO 
{
    public string CurrentDirectory { get; private set; }

    public ContentFileIO()
    {
        CurrentDirectory = Application.persistentDataPath + "/GData";

        if (false == Directory.Exists(CurrentDirectory))
        {
            Directory.CreateDirectory(CurrentDirectory);
        }
    }

    public bool DoesFileExist(string filename)
    {
        return File.Exists(CurrentDirectory + "/" + filename);
    }

    public DateTime GetServerTimestamp(string filename)
    {
        string filePath = CurrentDirectory + "/" + filename;

        string timestamp = File.ReadAllText(filePath + ".ts");

        return DateTime.Parse(timestamp);

    }

    public void WriteToDisk(string filename, byte[] bytes, DateTime timestamp)
    {
        string filePath = CurrentDirectory + "/" + filename;

        using (FileStream fs = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
        {
            fs.Write(bytes, 0, bytes.Length);
        }

        using (StreamWriter writer = File.CreateText(filePath + ".ts"))
        {
            writer.WriteLine(timestamp.ToString());
        }
    }

    public Texture2D LoadFromDisk(string filename)
    {
        string filePath = CurrentDirectory + "/" + filename;

        byte[] bytes = File.ReadAllBytes(filePath);

        // Create a texture. Texture size does not matter, since
        // LoadImage will replace with with incoming image size.
        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(bytes);

        return tex;
    }
}