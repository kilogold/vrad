﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

/// <summary>
/// This class contains all analytics-related behavior.
/// It should listen to events from other modules like the Ad Show Controllers
/// in order to determine if/when an ad is presented/served.
/// </summary>
public class AdAnalyticsCapture : MonoBehaviour
{
    /// <summary>
    /// Enumerates the Google Analytics defined custom metric ID.
    /// May consider making this enum it's own module.
    /// </summary>
    public enum GACustomMetrics { IndividualAdViewTime = 1};

    //TODO: Abstract analytics member to a generic analytics class using Proxy Pattern so we can swap analytics platforms.
    //      Isolate debug output to said class.
    private GoogleAnalyticsV4 analytics;

    private AdBaseShowController adShowController;

    /// <summary>
    /// Source where we get project id from.
    /// </summary>
    private AdRepoPersistentData adRepoPersistentData;

    public bool DisplayDebug = false;

    /// <summary>
    /// Is the ProjectID (AdRepoPersistentData) associated to a valid Analytics project entry
    /// in our database? Must be true in order to initiate analytics capture.
    /// </summary>
    private static bool isAuthenticated = false;

    /// <summary>
    /// Have we attempted authenticating? We don't have to authenticate for every instance of
    /// AdAnalyticsCapture... We can just cache the results of authentication and reuse for future 
    /// instances of analytics capture.
    /// </summary>
    private static bool didAttemptedAuthentication = false;

    /// <summary>
    /// Perform proccess of authentication during initial run.
    /// </summary>
    /// <returns></returns>
    IEnumerator Start()
    {
        if (didAttemptedAuthentication)
            yield break;

        didAttemptedAuthentication = true;
        WWW validateProjectID = new WWW("http://54.201.100.73/check_projectid.php?pid=" + adRepoPersistentData.ProjectID);
        yield return validateProjectID;

        isAuthenticated = (validateProjectID.text == "valid");

        validateProjectID.Dispose();

        if( isAuthenticated )
        {
            Debug.Log("Valid ProjectID. Initiating analytics capture.");
        }
        else
        {
            Debug.LogError("Invalid ProjectID (" + adRepoPersistentData.ProjectID + "). No analytics data will be captured in this session.");
        }
    }

    void Awake()
    {
        adShowController = GetComponent<AdBaseShowController>();
        analytics = GoogleAnalyticsV4.instance;
        adRepoPersistentData = Resources.Load<AdRepoPersistentData>("ScriptableObjects/AdRepoData");
    }

    void OnEnable()
    {
        adShowController.OnDisplayNewContent += LogNewContentDisplayed;
    }

    void OnDisable()
    {
        adShowController.OnDisplayNewContent -= LogNewContentDisplayed;
    }

    public void LogNewContentDisplayed()
    {
        // Log metric of newly displayed ad.
        string eventLabel = adShowController.DisplayedAd.Info.name +
            " - " + adShowController.DisplayedAd.Info.uniqueID;

        analytics.LogEvent("Advertisement", "Displayed", eventLabel, 1);

        if( DisplayDebug )
        {
            Debug.Log("<color=Magenta>" + eventLabel + "</color>");
        }
    }

    private string LogZoneEvent(string behaviortag, string projectID, string adUid, int zoneUid, float durationInSeconds = -1 )
    {
        string eventCategoryObserved = "Zone";
        string eventAction = behaviortag + "-" + adRepoPersistentData.ProjectID + ":" + adShowController.GetInstanceID();
        string eventLabel = adShowController.DisplayedAd.Info.uniqueID;
        long eventvalue = (long)(durationInSeconds * 1000); //convert to milliseconds.

        analytics.LogEvent(eventCategoryObserved, eventAction, eventLabel, eventvalue);

        //string eventCategory = "Zone";
        //string eventAction = behaviortag + "-" + adRepoPersistentData.ProjectID + ":" + adShowController.GetInstanceID();
        //string eventLabel = adShowController.DisplayedAd.Info.uniqueID;
        //uint eventMilliseconds = (uint)(durationInSeconds * 1000); //convert to milliseconds.


        //EventHitBuilder builder = new EventHitBuilder()
        //.SetEventCategory(eventCategory)
        //.SetEventAction(eventAction)
        //.SetEventLabel(eventLabel)
        //.SetEventValue(-1)
        //.SetCustomMetric((int)GACustomMetrics.IndividualAdViewTime, eventMilliseconds.ToString());

        //analytics.LogEvent(builder);

        return "[" + eventCategoryObserved + "] " + eventAction + "---" + eventLabel;
    }

    public void LogImpression(float duration)
    {
        string debugOutputConsumed = LogZoneEvent(
                                "Consumed",
                                adRepoPersistentData.ProjectID,
                                adShowController.DisplayedAd.Info.uniqueID,
                                adShowController.GetInstanceID(),
                                duration);

                    if (DisplayDebug)
                    {
                        Debug.Log("<color=Brown>" + debugOutputConsumed + "</color>");
                    }
    }

    public void LogGaze(float duration)
    {
        string debugOutputObserved = LogZoneEvent(
                                        "Observed",
                                        adRepoPersistentData.ProjectID,
                                        adShowController.DisplayedAd.Info.uniqueID,
                                        adShowController.GetInstanceID(),
                                        duration);

        if (DisplayDebug)
        {
            Debug.Log("<color=Brown>" + debugOutputObserved + "</color>");
        }
    }
}
