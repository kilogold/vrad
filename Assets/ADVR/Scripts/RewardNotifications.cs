﻿using UnityEngine;
using System.Collections;
using System;

public class RewardNotifications : MonoBehaviour
{
    public static event Action OnAdReward;

	// Use this for initialization
	private void Start ()
    {
        var showControllers = FindObjectsOfType<AdBaseShowController>();
        foreach (var item in showControllers)
        {
            item.Interpreter.OnImpression += ImpressionToReward;
        }
	}

    void OnDestroy()
    {
        var showControllers = FindObjectsOfType<AdBaseShowController>();
        foreach (var item in showControllers)
        {
            item.Interpreter.OnImpression -= ImpressionToReward;
        }
    }

    private void ImpressionToReward(float d)
    {
        if(OnAdReward != null)
        {
            OnAdReward();
        }
    }
}
