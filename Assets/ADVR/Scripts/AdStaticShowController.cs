using UnityEngine;
using System.Collections;

public class AdStaticShowController : AdBaseShowController
{

    void Start()
    {
        model.AdRepoInitialized += delegate { StartCoroutine(Initialize()); };
    }

    // Use this for initialization
    private IEnumerator Initialize()
    {

        if (adType == MediaType.Movie)
        {
            var mp = gameObject.AddComponent<MediaPlayerCtrl>();
            mp.m_TargetMaterial = new GameObject[1] { gameObject };
            mp.m_bFullScreen = false;
            mp.m_bSupportRockchip = true;
            mp.m_ScaleValue = MediaPlayerCtrl.MEDIA_SCALE.SCALE_Y_TO_X;
            mp.m_objResize = new GameObject[1] { gameObject };
            mp.m_bLoop = true;
            mp.m_bAutoPlay = true;
        }

        while (null == DisplayedAd)
        {
            switch (adType)
            {
                case MediaType.Movie:
                    DisplayedAd = model.GenerateMovieAd(theme, view.material,100.0f, GetComponent<MediaPlayerCtrl>());
                    break;

                case MediaType.Texture:
                    DisplayedAd = model.GenerateTextureAd(theme, view.material, 100.0f);
                    break;

                default:
                    break;
            }

            yield return null;
        }

        DisplayedAd.Play();
        InvokeOnDisplayNewContentEvent();
    }

    // Update is called once per frame
    void Update()
    {
        if (null != DisplayedAd)
        {
            DisplayedAd.Tick(Time.deltaTime);
        }
    }
}
