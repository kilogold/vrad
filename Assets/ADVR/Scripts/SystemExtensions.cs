namespace System
{
    public delegate void ActionRef<T>(ref T item1);
    public delegate void ActionRef<T, R>(ref T item1, ref R item2);
}
