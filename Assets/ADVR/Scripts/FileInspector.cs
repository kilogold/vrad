﻿using UnityEngine;
using System.Collections;
using System.IO;

/// <summary>
/// Inspects file information.
/// Has nothing to do with Unity's inspector.
/// </summary>
public static class FileInspector 
{
    public static MediaType GetMediaType(string filepath)
    {
        string extension = Path.GetExtension(filepath).ToLower();

        switch (extension)
        {
            case ".mp4":
                return MediaType.Movie;

            case ".jpg":
            case ".png":
            case ".bmp":
                return MediaType.Texture;

            default:
                Debug.LogError("Unidentified file type: " + extension);
                return MediaType.INVALID;
        }
    }
}
