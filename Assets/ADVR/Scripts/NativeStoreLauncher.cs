﻿using UnityEngine;
using System.Collections;

public static class NativeStoreLauncher
{
    private static AndroidJavaClass StoreLauncherInstance;
    private static AndroidJavaObject unityActivity;

    private static bool IsInitialized()
    {
        return (StoreLauncherInstance != null && unityActivity != null);
    }

    private static void Init()
    {
        AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        unityActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
        StoreLauncherInstance = new AndroidJavaClass("com.gumdrop.managedandroid.StoreLauncher");
    }

    public static void ShowListing(string appPackage)
    {
        if( false == IsInitialized() ) { Init(); }

        StoreLauncherInstance.CallStatic("LaunchStore", unityActivity, appPackage);
    }
}
