﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class GumdropWindowEditor : EditorWindow
{
    private Texture2D logo;
    private GUIStyle logoStyle;
    private string projectID;

    [MenuItem("Gumdrop/Main")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(GumdropWindowEditor),false,"Gumdrop");
    }

    void OnEnable()
    {
        if (logo == null)
        {
            //string logoFilepath = AssetDatabase.FindAssets("gdLogo t:texture2D")[0];
            //Debug.Log("logoFilepath: " + logoFilepath);

            logo = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/ADVR/Textures/Editor/gdLogo.jpg");
            Debug.Log(logo != null);

            logoStyle = new GUIStyle();
            logoStyle.alignment = TextAnchor.UpperCenter;
        }
    }

    void OnGUI()
    {
        // The actual window code goes here
        GUILayout.BeginVertical();
        GUILayout.Label(logo, logoStyle);
        GUILayout.Label("Gumdrop Inc.", logoStyle);
        GUILayout.Space(30);

        projectID = EditorGUILayout.TextField(
            new GUIContent("Project ID:",
            "A unique name id to differentiate it from all other zones in your project."),
            projectID);

        GUILayout.Space(30);

        GUILayout.Button("Connect");
        GUILayout.EndVertical();
    }
}
