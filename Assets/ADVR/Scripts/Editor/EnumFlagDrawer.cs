﻿// based on http://wiki.unity3d.com/index.php/EnumFlagPropertyDrawer
// simplified by Nolan Baker
// licensed under Creative Commons Attribution Share Alike

using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;

[CustomPropertyDrawer(typeof(EnumFlagAttribute))]
public class EnumFlagDrawer : PropertyDrawer
{
    const float rows = 2;  // total number of rows

    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        //Slice or fracionalize the height by the amount of rows.
        rect.height = rect.height / rows;

        if (property.propertyType == SerializedPropertyType.Enum)
        {
            property.intValue = EditorGUI.MaskField(rect, label, property.intValue, property.enumNames);

            //Offset by a row's height.
            rect.y += rect.height;

            property.intValue = Convert.ToInt32( EditorGUI.TextField( rect, "Bit Mask", Convert.ToString(property.intValue, 2) ),2 );
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * rows;
    }
}