﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEditor.Callbacks;

[CustomEditor(typeof(AdRepo))]
public class AdRepoEditor : Editor
{
    AdRepoPersistentData adRepoPersData;
    SerializedProperty propContentLoaders;
    SerializedProperty propShowDebug;

    void OnEnable()
    {
        adRepoPersData = Resources.Load<AdRepoPersistentData>("ScriptableObjects/AdRepoData");
        propContentLoaders = serializedObject.FindProperty("contentLoaders");
        propShowDebug = serializedObject.FindProperty("DisplayDebug");
    }

    public override void OnInspectorGUI()
    {
        adRepoPersData.ProjectID = EditorGUILayout.TextField("Project ID", adRepoPersData.ProjectID);
        EditorGUILayout.PropertyField(propContentLoaders,true);
        EditorGUILayout.PropertyField(propShowDebug);

        serializedObject.ApplyModifiedProperties();
    }
}
