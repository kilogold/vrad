using UnityEngine;
using System.Collections;
using System;

public class MovieAd : Ad
{
    MediaPlayerCtrl mediaPlayer;
    AdCore adCore;

    AdContentInfo Ad.Info
    {
        get { return adCore.AdContent.Info; }
    }

    event Action Ad.AdExpired
    {
        add { adCore.AdExpired += value; }
        remove { adCore.AdExpired -= value; }
    }

    public MovieAd(AdContentMovie adContentIn, Material adSurfaceMaterialIn,
                    float lifetime, MediaPlayerCtrl mediaPlayerControlIn)
    {
        adCore = new AdCore(adContentIn, adSurfaceMaterialIn, lifetime);
        mediaPlayer = mediaPlayerControlIn;
        mediaPlayer.Load(adCore.AdContent.Info.filepath);
    }

    public void Play()
    {
        adCore.Play();
        mediaPlayer.Play();
    }

    public void Stop()
    {
        adCore.Stop();
        mediaPlayer.Stop();
    }

    public void Pause()
    {
        adCore.Pause();
        mediaPlayer.Pause();
    }

    public void Tick(float deltaTime)
    {
        adCore.Tick(deltaTime);
    }
}
