using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

// Central location to cache all ad resources.
public class AdRepo : Singleton<AdRepo>
{
    /// <summary>
    /// Central container/cache of indices and references to actual content data.
    /// TODO:   Consider loading data upon runtime to reduce memory consumption from cache.
    ///         Alternatively consider storing the caching/indexing mechanism on storage somehow.
    /// </summary>
    private List<AdContent> AdCache = new List<AdContent>();

    /// <summary>
    /// Design-time assignment of content loaders for the adrepo.
    /// At the time of writing, we are only using Dropbox, but can support multiple sources.
    /// </summary>
    [SerializeField]
    private ContentLoader[] contentLoaders;

    /// <summary>
    /// Event to fire when the AdRepo has loaded all the initial
    /// content from the content loaders. At this point, the AdRepo
    /// is ready to provide ads to the surfaces.
    /// </summary>
    public event Action AdRepoInitialized;

    /// <summary>
    /// Flag to avoid re-initialization.
    /// </summary>
    private bool isAdRepoInitialized = false;

    /// <summary>
    /// Flag to keep track of sequential content loader initialization.
    /// Refer to Start().
    /// </summary>
    private bool isReadyToLoadContent = true;

    /// <summary>
    /// The count of how many content loaders have been initialized so far.
    /// </summary>
    private int contentLoaderInitCount;

    /// <summary>
    /// Retrieves content from file when loading new content from disk to cache.
    /// </summary>
    private ContentFileIO fileIO;

    /// <summary>
    /// Whether to output debug info to the log.
    /// </summary>
    public bool DisplayDebug = false;

    /// <summary>
    /// Key: AdContent unique id
    /// Value: Associated texture
    /// </summary>
    Dictionary<string, Texture2D> loadedTextures = new Dictionary<string, Texture2D>();

    void Awake()
    {
        fileIO = new ContentFileIO();
    }

    IEnumerator Start()
    {
        for (int curLoaderIndex = 0; curLoaderIndex < contentLoaders.Length;  )
        {
            // If we are already loading content...
            if( isReadyToLoadContent == false)
            {
                // Exit until the next frame
                yield return null;

                // Upon returning in the next frame, we start over.
                continue;
            }

            // At this point, we are ready to run the next content loader, so let's not allow any
            // other content loaders to execute until the current one is done.
            isReadyToLoadContent = false;

            // Content loading is performed after Start().
            GameObject contentLoaderInstance = Instantiate(contentLoaders[curLoaderIndex].gameObject) as GameObject;
            contentLoaderInstance.transform.parent = transform;

            // Grab the instance's ContentLoader script component, and register
            // to the 'ContentLoadingCompleteEvent'.
            // When we get the callback from the event, we can destroy the contentloader game object
            // if we wish.
            ContentLoader contentLoaderScript = contentLoaderInstance.GetComponent<ContentLoader>();
            contentLoaderScript.ContentLoadingCompleteEvent += OnInternalContentLoadingCompleteEvent;
            contentLoaderScript.WillLoadEvent += OnWillLoad;

            // Manually increment the loop index
            ++curLoaderIndex;
        }
    }

    /// <summary>
    /// Event callback for when a content manager will load. Essentially, a gatekeeper to determine if it should
    /// allow the content loader to continue.
    /// A particular scenario for this is to check if for example, the content loader has to download content...
    /// We can check if we already have the content loaded to avoid the overhead of the download.
    /// </summary>
    /// <param name="shouldLoad"></param>
    /// <param name="contentUniqueID"></param>
    void OnWillLoad(ref bool shouldLoad, ref string contentUniqueID)
    {
        // Can't use ref/out variables within lambda expression.
        string lambdaHack = contentUniqueID;
        AdContent searchResult = AdCache.Find(n => n.Info.uniqueID == lambdaHack);

        // Should load if no result is found.
        shouldLoad = (null == searchResult);

    }

    /// <summary>
    /// Method is called every time a content loader concludes processing.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="eventArgs"></param>
    void OnInternalContentLoadingCompleteEvent(object sender, ContentLoadingCompleteEventArgs eventArgs)
    {
        // Unregister the event, we don't need it anymore.
        // ACTUALLY... We want to load more in the future... When DO we want to unregister? OnDestroy?
        //((ContentLoader)sender).ContentLoadingCompleteEvent -= OnInternalContentLoadingCompleteEvent;

        // Grab the loaded content
        int textureAdCachePreviousCount = AdCache.Count;
        for (int curTextureIndex = 0; curTextureIndex < eventArgs.FilenameList.Length; curTextureIndex++)
        {
            bool isVideo = FileInspector.GetMediaType(eventArgs.FilenameList[curTextureIndex]) == MediaType.Movie;

            AdContent newEntry;
            
            if(isVideo)
            {
                newEntry = new AdContentMovie(
                        FilenameToFilepath(eventArgs.FilenameList[curTextureIndex]),
                        eventArgs.UrlList[curTextureIndex],
                        eventArgs.TitleList[curTextureIndex],
                        eventArgs.UniqueIdList[curTextureIndex],
                        eventArgs.ThemeMaskList[curTextureIndex] );
            } 
            else
            {
                string uniqueID = eventArgs.UniqueIdList[curTextureIndex];

                if (false == loadedTextures.ContainsKey(uniqueID))
                {
                    Texture2D newTexture = fileIO.LoadFromDisk(eventArgs.FilenameList[curTextureIndex]);
                    loadedTextures.Add(uniqueID, newTexture);
                }
                
                newEntry = new AdContentTexture(
                     loadedTextures[uniqueID],
                     FilenameToFilepath(eventArgs.FilenameList[curTextureIndex]),
                     eventArgs.UrlList[curTextureIndex],
                     eventArgs.TitleList[curTextureIndex],
                     uniqueID,
                     eventArgs.ThemeMaskList[curTextureIndex]);
            }


            AdCache.Add(newEntry);
        }

        if (DisplayDebug && AdCache.Count > textureAdCachePreviousCount)
        {
            int totalNewLoaded = AdCache.Count - textureAdCachePreviousCount;
            Debug.Log("<color=lime>" + totalNewLoaded + " new textures loaded!</color>");

            for (int i = textureAdCachePreviousCount; i < AdCache.Count; ++i )
            {
                Debug.Log("<color=yellow>" + AdCache[i].Info.name + " : " + AdCache[i].Info.url + "</color>");
            }
        }

        isReadyToLoadContent = true;
        ++contentLoaderInitCount;

        // If we've initialized all content loaders...
        // Should only happen once per repo init.
        if (contentLoaderInitCount >= contentLoaders.Length && isAdRepoInitialized == false )
        {
            if( AdRepoInitialized != null)
                AdRepoInitialized();

            isAdRepoInitialized = true;
        }
    }

    public MovieAd GenerateMovieAd(AdTheme theme, Material adSurfaceMaterial, float lifetime, MediaPlayerCtrl mediaPlayerInstance)
    {
        if (null != AdCache && AdCache.Count > 0 )
        {
            // Get a movie AdContent
            var matches = AdCache.FindAll(e => FindMatchesByCriteria(e, (int)theme, MediaType.Movie));

            if (null != matches && matches.Count > 0)
            {
                int selectedIndex = UnityEngine.Random.Range(0, matches.Count - 1);
                AdContentMovie movieAdContent = matches[selectedIndex] as AdContentMovie;
                return new MovieAd(movieAdContent, adSurfaceMaterial, lifetime, mediaPlayerInstance);
            }
        }

        return null;
    }

    public TextureAd GenerateTextureAd(AdTheme theme, Material adSurfaceMaterial, float lifetime)
    {
        // Get a random texture AdContent
        AdContentTexture newAdContent = null;

        if (null != AdCache && AdCache.Count > 0)
        {
            //Generate an index to a texture that matches the criteria
            var matches = AdCache.FindAll(e => FindMatchesByCriteria(e, (int)theme, MediaType.Texture));

            // If we didn't find a criteria match, return failure.
            if (matches.Count == 0)
                return null;

            int listIndex = UnityEngine.Random.Range(0, matches.Count - 1);

            newAdContent = matches[listIndex] as AdContentTexture;
        }

        // Create a TextureAd with obtained AdContent
        if (null != newAdContent)
        {
            return new TextureAd(newAdContent, adSurfaceMaterial, lifetime);
        }

        return null;
    }

    public TextureAd[] GenerateTextureAds(AdTheme theme, Material adSurfaceMaterial, float lifetime, int count)
    {
        TextureAd[] returnTextureAds = null;

        if (null != AdCache && AdCache.Count > 0)
        {
            //Generate a list of textures that matches the criteria
            var matches = AdCache.FindAll(e => FindMatchesByCriteria(e, (int)theme, MediaType.Texture));

            // If we didn't find a criteria match, return failure.
            if (matches.Count == 0)
            {
                Debug.LogError("No criteria match found for generating Texture Ads.");
                return null;
            }
            if (matches.Count < count)
            {
                Debug.LogError("Not enough unique Texture Ads to accomodate request.");
                return null;
            }

            // At this point, we have validated that we have enough capacity to deliver
            // the requested amount of texture ads. Let's allocate the return array.
            returnTextureAds = new TextureAd[count];

            // We are going to grab the items from the list in chronological order.
            // To guarantee randomness, we shuffle the list beforehand.
            matches.Shuffle();
            for (int i = 0; i < count; i++)
            {
                var newAdContent = matches[i] as AdContentTexture;

                // Create a TextureAd with obtained AdContent
                if (null != newAdContent)
                {
                    returnTextureAds[i] = new TextureAd(newAdContent, adSurfaceMaterial, lifetime);
                }
            }

            return returnTextureAds;
        }



        return null;
    }

    private bool FindMatchesByCriteria(AdContent comparedItem, int theme, MediaType type )
    {
        bool doesMaskMatch = (comparedItem.Info.themeMask & theme) > 0;
        bool doesTypeMatch = comparedItem.Info.contentType == type;

        return (doesMaskMatch && doesTypeMatch);
    }

    private string FilenameToFilepath(string filename)
    {
        return fileIO.CurrentDirectory + "/" + filename;
    }

    public int GetAvailableUniqueTextureAdCount()
    {
        return AdCache.FindAll(ad => ad.Info.contentType == MediaType.Texture).Count;
    }
}