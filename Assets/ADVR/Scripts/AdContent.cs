﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Content information
/// </summary>
public class AdContentInfo
{
    public MediaType contentType = MediaType.INVALID;
    public int themeMask = 0;
    public string filepath;
    public string url;
    public string name;
    public string uniqueID;

    private AdContentInfo() { /*Disabled default constructor*/ }

    public AdContentInfo(string filepathIn, string contentUrlIn, string contentNameIn, string contentUniqueIDIn, int contentThemeMaskIn)
    {
        contentType = FileInspector.GetMediaType(contentUrlIn);
        url = contentUrlIn;
        name = contentNameIn;
        uniqueID = contentUniqueIDIn;
        filepath = filepathIn;
        themeMask = contentThemeMaskIn;
    }
}

/// <summary>
/// When an Ad is loaded from a loader, the content is cached in the repo.
/// This class stores all data associated with the content itself.
/// </summary>
public interface AdContent
{
    AdContentInfo Info { get; }
}

/// <summary>
/// Placeholder class for any move-specific things.
/// </summary>
public class AdContentMovie : AdContent
{
    private AdContentInfo info;
    AdContentInfo AdContent.Info
    {
        get { return info; }
    }

    public AdContentMovie(  string filepathIn, 
                            string contentUrlIn,
                            string contentNameIn, 
                            string contentUniqueIDIn, 
                            int contentThemeMaskIn )
    {
        /************************************************************************************************
         * EasyMovieTexture has a weird quirk where if we play a video from "C:/video.mp4", it will 
         * expect instead a "C://video.mp4". 
         * this is most likely due to the author assuming that the consumer of EasyMovieTexture library will
         * hardcode a path, which in C/C++/C# strings would require a "//" in order to represent a '/'.
         * Look in MediaPlayerCtrl::Call_Load() for details.
         * We could make the change in the plugin code, but then we have to redo the changes for every update.
         * Instead, we'll do a workaround here to mutate the filepath string into a EasyMovieTexture-acceptable
         * format.
         * TODO:
         * Turn AdRepo instantiation of AdContentMovie/Texture into 'AdContentFactory' to avoid all this 
         * messy preprocessor stuff.
         *************************************************************************************************/
#if UNITY_EDITOR_WIN
        filepathIn = filepathIn.Replace(":/", "://");
#elif UNITY_ANDROID
        filepathIn = filepathIn.Insert(0, "file://");
#endif
        info = new AdContentInfo(filepathIn, contentUrlIn, contentNameIn, contentUniqueIDIn, contentThemeMaskIn);
    }
}

/// <summary>
/// Texture-specific AdContent
/// </summary>
public class AdContentTexture : AdContent
{
    private AdContentInfo info;
    AdContentInfo AdContent.Info
    {
        get { return info; }
    }

    public Texture ContentTexture { get; private set; }

    public AdContentTexture( Texture contentIn, 
                             string filepathIn, 
                             string contentUrlIn, 
                             string contentNameIn, 
                             string contentUniqueIDIn, 
                             int contentThemeMaskIn )
    {
        ContentTexture = contentIn;

        info = new AdContentInfo(filepathIn, contentUrlIn, contentNameIn, contentUniqueIDIn, contentThemeMaskIn);
    }
}
