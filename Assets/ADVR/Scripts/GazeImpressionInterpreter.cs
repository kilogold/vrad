﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using System;

public class GazeImpressionInterpreter
{
    private VRInteractiveItem interaction;
    private const float INVALID_TIMESTAMP = -1;
    private float gazeStartTimestamp = INVALID_TIMESTAMP;
    private AdBaseShowController adShowController;

    public event Action<float> OnImpression;
    public event Action<float> OnGaze;


    /// <summary>
    /// How long shall we gaze until we consider a valid analytics
    /// entry depicting that the Ad was observed/consumed?
    /// </summary>
    public float impressionThresholdInSeconds = 3.0f;

    public GazeImpressionInterpreter(AdBaseShowController adShowControllerIn)
    {
        adShowController = adShowControllerIn;
        interaction = adShowController.GetComponent<VRInteractiveItem>();
    }

    public void OnEnable()
    {
        interaction.OnOut += Interaction_OnOut;
        interaction.OnOver += Interaction_OnOver;
        adShowController.OnDisplayNewContent += AdShowController_OnDisplayNewContent;
    }

    public void OnDisable()
    {
        adShowController.OnDisplayNewContent -= AdShowController_OnDisplayNewContent;
        interaction.OnOver -= Interaction_OnOver;
        interaction.OnOut -= Interaction_OnOut;
    }

    private void Interaction_OnOver()
    {
        // There's a chance we could be looking at an uninitialized/invalid ad surface.
        // We don't want to capture metrics if there is nothing being displayed.
        gazeStartTimestamp = (adShowController.DisplayedAd != null) ?
            Time.time : INVALID_TIMESTAMP;
    }

    private void Interaction_OnOut()
    {
        // If we don't have a valid beginning time stamp, we began gaze before we had an ad.
        // Furthermore, we are looking away while the surface has no ad. We can assert this because 
        // if an ad would have popped in, it would have been handled in the new-content-display event.
        // In conclusion, having an invalid start means we should not even process.
        if (gazeStartTimestamp == INVALID_TIMESTAMP)
            return;

        float gazeEndTimestamp = Time.time;
        float duration = gazeEndTimestamp - gazeStartTimestamp;

        // We want to report when an ad has been "consumed" as well as "observed".
        // Regardless if we meet the "consumed" criteria, we will report the "observed", as this way
        // we can interpret/infer analytics in more dimensions and info.
        OnGaze(duration);

        if (duration >= impressionThresholdInSeconds)
        {
            OnImpression(duration);
        }
    }

    private void AdShowController_OnDisplayNewContent()
    {
        /**********************************************************************************
        A show controller could alternate ads without us having to gaze away (SequenceShowcontroller).
        If an ad changes while we are still gazing, we want to capture the metric for the current ad 
        and provide for the this next incoming ad, therfore we restart the capture process within this method.
        **********************************************************************************/

        // If we are currently gazing...
        if (interaction.IsOver)
        {
            // If we were actually viewing an ad before this...
            if (gazeStartTimestamp != INVALID_TIMESTAMP)
            {
                // End & capture metric for the current ad.
                Interaction_OnOut();
            }
        }

        // Now that we have a new ad, let's begin metric capture for it.
        if (interaction.IsOver)
        {
            Interaction_OnOver();
        }
    }

}
