﻿using UnityEngine;
using System.Collections;
using System;

public class AdCore
{
    /************ FIELDS **********************/

    /// <summary>
    /// Time before the ad expires.
    /// </summary>
    protected float curLifetime;
    protected float maxLifetime;

    public event Action AdExpired;

    /************ PROPERTIES **********************/
    public bool IsPlaying { private set; get; }

    public bool IsExpired { get { return curLifetime <= 0; } }

    public AdContent AdContent { get; private set; }

    public Material AdSurfaceMaterial { get; private set; }

    public Texture OriginalTexture { get; private set; }

    /************ METHODS **********************/
    public AdCore(AdContent adContentIn, Material adSurfaceMaterialIn, float lifetime)
    {
        AdContent = adContentIn;
        AdSurfaceMaterial = adSurfaceMaterialIn;
        OriginalTexture = AdSurfaceMaterial.mainTexture;
        maxLifetime = curLifetime = lifetime;
    }

    public void Play()
    {
        IsPlaying = true;
    }

    public void Stop()
    {
        IsPlaying = false;
        curLifetime = maxLifetime;
    }

    public void Pause()
    {
        IsPlaying = false;
    }

    public void Tick(float deltaTime)
    {
        if (IsPlaying)
        {
            curLifetime -= deltaTime;

            if (IsExpired)
            {
                Pause();

                if (AdExpired != null)
                {
                    AdExpired();
                }
            }
        }
    }
}