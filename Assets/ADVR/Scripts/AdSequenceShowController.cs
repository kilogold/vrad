using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// This class manages the display of multiple ads in sequence.
/// Whether in time, or triggers (to be implemented), this sequence
/// show controller cycles through the multiple ads to be displayed.
/// </summary>
public class AdSequenceShowController : AdBaseShowController
{
    [SerializeField]
    private List<Ad> displayedAds;

    [SerializeField, Tooltip("How many ads to display in the sequence")]
    private int sequenceLength;

    /// <summary>
    /// The ad in the sequence that is currently being displayed.
    /// </summary>
    private int curSequenceAdIndex;

    [SerializeField]
    private bool isLooping;

    [SerializeField]
    private float AdCycleLifetime;

    void Start()
    {
        displayedAds = new List<Ad>(sequenceLength);
        model.AdRepoInitialized += delegate { StartCoroutine(Initialize()); };
    }


    // Use this for initialization
    private IEnumerator Initialize()
    {
        // We don't know if the sequence length is larger than the amount of ads we have available.
        // If it is, we will adjust the sequenceLength accordingly.
        int totalUniqueTextures = model.GetAvailableUniqueTextureAdCount();
        sequenceLength = Math.Min(sequenceLength, totalUniqueTextures);

        bool isInitializing = true;
        while( isInitializing )
        {
            switch (adType)
            {
                case MediaType.Movie:
                    {
                        //MovieAd movieAd = model.GenerateMovieAd(view.material, 5.0f, false);
                        //if (null != movieAd)
                        //{
                        //    displayedAds.Add(movieAd);
                        //    curAdIndx++;
                        //}
                    }
                    break;

                case MediaType.Texture:
                    {
                        TextureAd[] textureAds = model.GenerateTextureAds(theme, view.material, AdCycleLifetime,sequenceLength);

                        if (null != textureAds)
                        {
                            displayedAds.AddRange(textureAds);
                            isInitializing = false;
                        }
                    }
                    break;
                default:
                    break;
            }

            yield return null;
        }

        curSequenceAdIndex = 0;
        DisplayedAd = displayedAds[curSequenceAdIndex];
        InitAdDisplay();
    }

    private bool AnyNullAds()
    {
        foreach (var item in displayedAds)
        {
            if (item == null)
                return true;
        }

        return false;
    }

    void OnAdExpired()
    {
        DisplayedAd.Stop();
        DisplayedAd.AdExpired -= OnAdExpired;

        curSequenceAdIndex++;
        if (curSequenceAdIndex >= sequenceLength)
        {
            if (isLooping == false)
                return;


            curSequenceAdIndex = 0;
        }

        DisplayedAd = displayedAds[curSequenceAdIndex];
        InitAdDisplay();
    }

    /// <summary>
    /// Helper function to centralize the init of an Ad display.
    /// </summary>
    private void InitAdDisplay()
    {
        DisplayedAd.AdExpired += OnAdExpired;
        DisplayedAd.Play();

        InvokeOnDisplayNewContentEvent();
    }

    // Update is called once per frame
    void Update()
    {
        // Only false when we are not looping.
        // Consider set architecture that implies this.
        if (DisplayedAd != null && curSequenceAdIndex < displayedAds.Count)
        {
            DisplayedAd.Tick(Time.deltaTime);
        }
    }
}
