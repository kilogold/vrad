﻿using UnityEngine;
using System.Collections;

public class AdCallToActionDisplay : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        SetVisibility(false);
    }

    public void OnTriggerExit(Collider other)
    {
        SetVisibility(false);
    }

    public void OnTriggerEnter(Collider other)
    {
        SetVisibility(true);
    }

    private void SetVisibility(bool isVisible)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(isVisible);
        }
    }
}
