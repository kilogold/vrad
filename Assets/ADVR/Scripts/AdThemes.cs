﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Intended to be used as bitmask values.
/// </summary>
public enum AdTheme
{
    Happy =         1 << 0,
    Sad =           1 << 1,
    Dark =          1 << 2,
    Sexy =          1 << 3,
    Fun =           1 << 4,
    Angry =         1 << 5,
    Family =        1 << 6,
    Romance =       1 << 7,
    Nature =        1 << 8,
    Help =          1 << 9,
    Funny =         1 << 10,
    Travel =        1 << 11,
    Games =         1 << 12,
    Movies =        1 << 13,
    Finance =       1 << 14,
    Home =          1 << 15,
}
