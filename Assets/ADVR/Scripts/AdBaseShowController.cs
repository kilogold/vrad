﻿using UnityEngine;
using System.Collections;
using System;
using VRStandardAssets.Utils;

/// <summary>
/// Common base class interface for anything related to the ad show controllers.
/// We can always know which ad is being served via this class/interface.
/// </summary>
public abstract class AdBaseShowController : MonoBehaviour 
{
    // The reason this class isn't a pure interface is because the usage is simple enough where
    // encapsulation approaches would only result in extra code work with little to no additional
    // benefit. As this class grows more complex, we should revisit if encapsulation is required.

    public event Action OnDisplayNewContent;
    private AdAnalyticsCapture analytics;
    public GazeImpressionInterpreter Interpreter { private set; get; }

    public Ad DisplayedAd { protected set; get; }

    /// <summary>
    /// The renderer where the ad will be placed.
    /// This is also known as the ad surface.
    /// </summary>
    [SerializeField]
    protected Renderer view;

    /// <summary>
    /// Following the MVC pattern, here is the model, 
    /// whereas the controller is this class itself.
    /// </summary>
    protected AdRepo model;

    [SerializeField]
    protected MediaType adType;

    [SerializeField, EnumFlag]
    protected AdTheme theme;

    [SerializeField]
    protected string zoneNameID;

    protected void InvokeOnDisplayNewContentEvent()
    {
        if (OnDisplayNewContent != null)
            OnDisplayNewContent();
    }

    public void Awake()
    {
        model = AdRepo.Instance;
        analytics = GetComponent<AdAnalyticsCapture>();
        Interpreter = new GazeImpressionInterpreter(this);
    }

    public void OnEnable()
    {
        Interpreter.OnEnable();
        Interpreter.OnGaze += analytics.LogGaze;
        Interpreter.OnImpression += analytics.LogImpression;
    }

    public void OnDisable()
    {
        Interpreter.OnDisable();
        Interpreter.OnGaze -= analytics.LogGaze;
        Interpreter.OnImpression -= analytics.LogImpression;
    }

}