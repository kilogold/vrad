﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

public class HackVRButton : MonoBehaviour
{
    private VRInteractiveItem interaction;
    private float curViewTime;
    public float maxViewTime = 2.0f;
    bool didLaunch = false;

    // Use this for initialization
    void Start()
    {
        interaction = GetComponent<VRInteractiveItem>();
        interaction.OnOver += Interaction_OnOver;
        interaction.OnOut += Interaction_OnOut; ; 
    }

    private void Interaction_OnOut()
    {
    }

    void Update()
    {
        if(!didLaunch && interaction.IsOver && Time.time - curViewTime > maxViewTime)
        {
            NativeStoreLauncher.ShowListing("com.adobe.fas");
            didLaunch = true;
        }
    }

    private void Interaction_OnOver()
    {
        curViewTime = Time.time;
        didLaunch = false;
    }
}
