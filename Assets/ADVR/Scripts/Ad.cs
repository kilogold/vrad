using UnityEngine;
using System.Collections;
using System;

public enum MediaType
{
    INVALID,
    Movie,
    Texture
}

/// <summary>
/// Base class for the Ad mechanism. Subclasses extend the Play/Stop methods
/// to perform the actual texture swap. This class primarily maintains common 
/// members across subclasses, as well as expiration logic. 
/// </summary>
public interface Ad
{
    void Play();

    void Stop();

    void Pause();

    void Tick(float deltaTime);

    AdContentInfo Info { get;}

    /// <summary>
    /// Mostly intended as a wrapper class for Movie and Texure ads.
    /// The implementing class will forward operations to it's internal 
    /// event invoked by an AdCore instance.
    /// </summary>
    event Action AdExpired;
}
