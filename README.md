# Welcome

VRAD is an Ad-placement platform for Virtual Reality environments.  
Although it can run on non-VR environments, the api is specifically dedicated to this particular endeavor.

# Major Components

**AdRepo -**  
MonoBehaviour Component that lives on the hierarchy.
It is in charge of obtaining ads to play on the ad surfaces.

**AdStaticShowController -**  
Controller for displaying a single ad.

**AdSequenceShowController -**  
Controller for displaying a sequence of ads.


# Repo Operation

When operating for push/pull/commit/etc, we make sure that we honor the subtree we are managing.
VRAD is a platform that is meant to be intergrated into other projects, preferably via ".dll" boxed in ".unitypackage" objects.

To mature, test, and develop VRAD, we must sometimes import actual source code.  
To do so, we use git subtrees.  
Definitions in order:  
  
*  **Mother** - ADVR repo  
*  **Child** - Project receiving the ADVR code.  

### Requisites  

Mother's "backport" branch is very key to the subtree approach.  
The backport is the communication point between Mother and Child.  
Mother and Child must have Assets/ADVR as a Git subtree.  
The subtree would be a separate "repo" (more like history/branch), which is formally tracked by backport. Thus, the importance of backport, as it is where the "exportable" code lives. 

Child **must** add Mother's remote in order to operate for subtree.
Once remote has been added to Child, the following operations are availble.
  
### Mother Operations  

master to backport (from master branch)
```
$ git subtree split -P Assets/ADVR -b backport --squash
```

backport to master (from master branch)
```
$ git subtree merge -P Assets/ADVR backport --squash
```

### Child Operations 

send changes to mother (**IMPORTANT**: KEEP COMMITS FOR BACKPORT DIRECTORY SEPARATE BEFORE PUSH!!!)
```
$ git subtree push --prefix Assets/ADVR VRAD backport --squash
```

receive changes from mother
```
$ git subtree pull --prefix Assets/ADVR VRAD backport --squash
```